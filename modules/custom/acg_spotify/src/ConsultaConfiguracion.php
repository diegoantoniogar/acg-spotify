<?php

namespace Drupal\acg_spotify;

class ConsultaConfiguracion {

  public static function obtenerConfiguracion () {
    $configuracion = \Drupal::config('acg_spotify.settings');
    $configuracion = $configuracion -> get();
    return $configuracion;
  }

  public static function actualizarConfiguracion ($llave, $valor) {
    $configuracion = \Drupal::configFactory() -> getEditable('acg_spotify.settings');
    $configuracion -> set ($llave, $valor);
    $configuracion -> save();
    $actualizados = ConsultaConfiguracion::obtenerConfiguracion();
    return $actualizados;
  }
}
