<?php

namespace Drupal\acg_spotify\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\acg_spotify\ConsultaConfiguracion;
use Drupal\acg_spotify\ConsultaAPI;

class NavegacionController extends ControllerBase {

  public static function informacion () {
    $configuracion = ConsultaConfiguracion::obtenerConfiguracion();
    return array(
      'limite' => $configuracion['acg_spotify__limite'],
      'pais' => $configuracion['acg_spotify__pais'],
      'api' => new ConsultaAPI(),
    );
  }

  public static function ultimosLanzamientos () {
    $informacion    = NavegacionController::informacion();
    $contenido      = array();
    $lanzamientos   = $informacion['api'] -> ultimos_lanzamientos(
      $informacion['limite'],
      $informacion['pais']
    );
    $contenido[]    = [
      '#theme'                    => 'ultimos_lanzamientos_template',
      '#lanzamientos'             => $lanzamientos
    ];
    return $contenido;
  }

  public function artista ($id_artista) {
    $informacion  = NavegacionController::informacion();
    $contenido    = array();
    $artista      =  $informacion['api'] -> artista($id_artista);
    $canciones    =  $informacion['api'] -> top_canciones(
      $id_artista,
      $informacion['pais']
    );
    $contenido[]  = [
      '#theme'                    => 'artista_template',
      '#artista'                  => $artista,
      '#canciones'                => $canciones
    ];
    return $contenido;
  }
}
