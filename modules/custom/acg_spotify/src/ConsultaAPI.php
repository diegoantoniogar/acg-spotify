<?php

namespace Drupal\acg_spotify;
use Drupal\acg_spotify\ConsultaConfiguracion;

class ConsultaAPI {

  public $token;

  public function __construct () {
    $this -> token = $this -> obtener_token();
    $this -> API = 'https://api.spotify.com/v1/';
  }

  public function obtener_token () {
    $configuracion = ConsultaConfiguracion::obtenerConfiguracion();
    $client_id = $configuracion['acg_spotify__cliend_id']; 
    $client_secret = $configuracion['acg_spotify__client_secret']; 
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL,            'https://accounts.spotify.com/api/token' );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($curl, CURLOPT_POST,           1 );
    curl_setopt($curl, CURLOPT_POSTFIELDS,     'grant_type=client_credentials' ); 
    curl_setopt($curl, CURLOPT_HTTPHEADER,     array('Authorization: Basic '.base64_encode($client_id.':'.$client_secret))); 
    $respuesta = curl_exec($curl);
    $respuesta = json_decode($respuesta);
    return $respuesta -> access_token;
  }

  public function ultimos_lanzamientos ($limite, $pais) {
    $url = 'browse/new-releases?country=' . $pais . '&limit=' . $limite;
    $consulta = $this -> consulta($url);
    $lanzamientos = array();
    foreach ($consulta -> albums -> items as $lanzamiento) {
      $lanzamientos[] = array(
        'img' => $lanzamiento -> images[1],
        'artistas' => $lanzamiento -> artists,
        'nombre' => $lanzamiento -> name
      );
    }
    return $lanzamientos;
  }


  public function top_canciones ($id_artista, $pais) {
    $url = 'artists/' . $id_artista . '/top-tracks?market=' . $pais;
    $consulta = $this -> consulta($url);
    $canciones = array();
    foreach ($consulta -> tracks as $cancion) {
      $canciones[] = array(
        'nombre_album' => $cancion -> album -> name,
        'foto_album' => $cancion -> album -> images[2],
        'nombre' => $cancion -> name
      );
    }
    return $canciones;
  }

  public function artista ($id_artista) {
    $url = 'artists?ids=' . $id_artista;
    $consulta = $this -> consulta($url);
    $informacion = array();
    foreach ($consulta -> artists as $artista) {
      $informacion['img'] = $artista -> images[0];
      $informacion['nombre'] = $artista -> name;
    }
    return $informacion;
  }

  public function consulta ($url) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $this -> API . $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer ' . $this -> token
      ),
    ));
    $respuesta = curl_exec($curl);
    $respuesta = json_decode($respuesta);
    curl_close($curl);
    return $respuesta;
  }
}
