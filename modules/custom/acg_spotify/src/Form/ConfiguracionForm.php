<?php

namespace Drupal\acg_spotify\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\acg_spotify\ConsultaConfiguracion;

class ConfiguracionForm extends FormBase {

  public function getFormId () {
    return 'acg_spotify__configuracion_form';
  }

  public function buildForm (array $form, FormStateInterface $form_state) {
    $defaults = ConsultaConfiguracion::obtenerConfiguracion();

    $form['conjunto'] = array(
      '#type'           => 'fieldset',
      '#title'          => $this ->t(
        'Parámetros para Spotify'
      ),
    );

    $form['conjunto']['acg_spotify__limite'] = array(
      '#type'           => 'number',
      '#title'          => $this->t('Número de resultados por página'),
      '#default_value'  => $defaults['acg_spotify__limite'],
    );

    $form['conjunto']['acg_spotify__pais'] = array(
      '#type'           => 'textfield',
      '#title'          => $this->t('Código de pais'),
      '#size' => 6,
      '#maxlength' => 6,
      '#default_value'  => $defaults['acg_spotify__pais'],
    );

    $form['conjunto']['acg_spotify__cliend_id'] = array(
      '#type'           => 'textfield',
      '#title'          => $this->t('Client ID'),
      '#size' => 60,
      '#maxlength' => 200,
      '#default_value'  => $defaults['acg_spotify__cliend_id'],
    );

    $form['conjunto']['acg_spotify__client_secret'] = array(
      '#type'           => 'textfield',
      '#title'          => $this->t('Client secret'),
      '#size' => 60,
      '#maxlength' => 200,
      '#default_value'  => $defaults['acg_spotify__client_secret'],
    );

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type'           => 'submit',
      '#value'          => $this->t('Guardar'),
      '#button_type'    => 'primary',
    );
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    ConsultaConfiguracion::actualizarConfiguracion(
      'acg_spotify__limite',
      $form_state -> getValue('acg_spotify__limite')
    );
    ConsultaConfiguracion::actualizarConfiguracion(
      'acg_spotify__pais',
      $form_state -> getValue('acg_spotify__pais')
    );
    ConsultaConfiguracion::actualizarConfiguracion(
      'acg_spotify__cliend_id',
      $form_state -> getValue('acg_spotify__cliend_id')
    );
    ConsultaConfiguracion::actualizarConfiguracion(
      'acg_spotify__client_secret',
      $form_state -> getValue('acg_spotify__client_secret')
    );

    drupal_set_message("Datos guardados");
    $form_state->setRedirect('acg_spotify.configuracion_form');
    \Drupal::service('router.builder')->rebuild();
  }
}

